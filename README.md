# Stream Demo
This project originally followed lessons from [Modern React with Redux](https://www.udemy.com/share/101WcYBkATdlhbQHg=/) 
and was then enhanced with my own modifications.

## Contents
**/client** - Contains the CRUD React app (bootstrapped with Create React App). 
Uses Semantic UI CSS styling. Uses Redux & Redux-Thunk for state management and 
middleware. 
**/api** - Contains the fake back-end API server. 
Using this library: [json-server](https://github.com/typicode/json-server)
**/rtmp** - Contains the Real Time Messaging Protocol server for streaming video. 
Using this library: [node-media-server](https://github.com/illuspas/Node-Media-Server/)

### How it runs:
For the basic CRUD functionality without video streaming, you need to run two 
separate terminals -- one for the client and one for the api. 
Each are started using `npm start`. In order to stream video, you will need a
third terminal to run in rtmp, also using npm start command.  

#### To demonstrate streaming your screen and sound to the app:
Using [OBS Studio](https://obsproject.com/) to record the screen and audio, set 
the stream to the following custom server (Settings->Stream->Custom->Server): 
`rtmp://localhost/live`, where the Stream Key is equal to the id from the stream, 
e.g. if your stream is http://localhost:3000/streams/1,  then the Stream Key 
would be 1.
Once all the terminals are up and running, click "Start Streaming" in OBS and 
navigate to the stream link, and once you hit play, you will see the video 
player reflect what you are streaming in OBS.