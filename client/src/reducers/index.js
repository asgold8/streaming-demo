import { combineReducers } from 'redux';
//renamed for descriptive clarity
import { reducer as formReducer } from 'redux-form';

import authReducer from './authReducer';
import streamReducers from './streamsReducer';

export default combineReducers ({
  auth: authReducer,
  form: formReducer,
  streams: streamReducers
});